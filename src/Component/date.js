
import React from 'react';


class Tarikh extends React.Component {

state = {date: ""};

// چرخه حیات مرحله پایان اجرای کامپوننت
componentDidMount(){
  this.setState({date:new Date().getMonth()});
}

render(){
{/* مقدار اوپس رو مساوی تاریخ میزاریم و تبدیل به استرینگ میکنیم */}
  let ops = "";
  if(this.state.date !== ""){
    ops = this.state.date.toString();
  }
  let tabestan = "tabestan";
  let zemestan = "zemastan";

  if (ops <= 6 ) {
    return (
      <div className="tabestan">
        <i className=" tabestan massive iconleft sun icon"></i>
        <h1 className="txtseason">{tabestan}</h1>
        <i className=" tabestan massive iconright sun icon"></i>
      </div>

    )
  }

  if (ops > 6) {
    return(
      <div className="zemestan">
        <i className=" zemestan massive iconleft snowflake icon"></i>
        <h1 className="txtseason">{zemestan}</h1>
        <i className=" zemestan massive iconright snowflake icon"></i>
      </div>
    )
  }

};
};



export default Tarikh;
